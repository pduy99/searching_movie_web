const API_KEY = "api_key=ffafbd7ad7beaef52e88f04b016c6a61"
const GET_MOVIE_API = "https://api.themoviedb.org/3/search/movie?";
const GET_TOP_RATED_API = "https://api.themoviedb.org/3/movie/top_rated?language=en-US";
const IMAGE_LINK = "https://image.tmdb.org/t/p/w500";
const GET_GENRE_API = "https://api.themoviedb.org/3/genre/movie/list?";
const GET_PERSON_API = "https://api.themoviedb.org/3/search/person?";
const GET_MOVIE_BY_ACTOR_API ="https://api.themoviedb.org/3/discover/movie?api_key=ffafbd7ad7beaef52e88f04b016c6a61&language=en-US&sort_by=vote_average.desc&with_cast="
const GET_MOVIE_DETAILS_API = "https://api.themoviedb.org/3/movie/";
const GET_ACTOR_DETAILS_API = "https://api.themoviedb.org/3/person/";
const GET_MOVIE_CREDITS_FOR_PERSON_API = "https://api.themoviedb.org/3/person/"


async function getMovieByName(selected_page)
{
    const strSearch = $('form input').val();
    var strRequest = `${GET_MOVIE_API}&${API_KEY}&query=${strSearch}&page=${selected_page}`;
    var response = await fetch(strRequest);
    var res = await response.json();
    return res;
}

async function getActorID()
{
    const strSearch = $('form input').val();
    var strRequest = `${GET_PERSON_API}&${API_KEY}&query=${strSearch}`;
    var response = await fetch(strRequest);
    var res = await response.json();
    if(res.total_results == 0)
    {
        return null;
    }
    return res.results[0].id;

}

async function getMovieByActor(selected_page)
{
    var id = await getActorID();
    if(id ==0 )
    {
        null;
    }else{
        var strRequest = `${GET_MOVIE_BY_ACTOR_API}${id}&page=${selected_page}`;
        var response = await fetch(strRequest);
        var res = await response.json();
        return res;
    }
}
async function getGenre(id)
{
    var strRequest = `${GET_GENRE_API}&${API_KEY}`;
    var response = await fetch(strRequest);
    var res = await response.json();
    var genre = res.genres;
    for(const index of genre)
    {
        if(index.id == id)
            return index.name;
    }
    
}

async function buildGenreString(genre_array)
{
    var res = "";
    for(var i = 0; i<genre_array.length;i++)
    {
        res += await getGenre(genre_array[i]);
        res += ", ";
    }
    return res;
}


async function showTopRated()
{
    var strRequest = `${GET_TOP_RATED_API}&${API_KEY}`;
    Loading();
    var response = await fetch(strRequest);
    var res = await response.json();
    var list_movie = res.results;
    $('#pagination').empty();
    $('#pagination').append(
        `
        <div col-md>
            <h1> Top Rated Movies </h1>
        </div>
        `
    )
    fillMovie(list_movie);
}


function Loading()
{
    $('#main').empty();
    $('#main').append(
        `
        <div class="loading">
            <div class="loader">
            </div>
        </div>
        `
    )
}

async function onClickSubmit(e)
{
    e.preventDefault();
    Loading();
    var searchActor = await getMovieByActor(1);
    var searchMovieName = await getMovieByName(1);
    if(searchActor != null && searchActor.total_results > searchMovieName.total_results)
    {
        showPagination(searchActor);
        var list_movie = searchActor.results;
        fillMovie(list_movie);
        $(".pagination li.current-page").on("click", async function () {
        
            selected_page = $(this).index();
            $('.pagination li').removeClass('active');
            $(this).addClass('active'); 
            Loading();
            var res = await getMovieByActor(selected_page);
            var list_movie = res.results;
            fillMovie(list_movie);
        });
        
    }
    else{
        showPagination(searchMovieName);
        var list_movie = searchMovieName.results;
        fillMovie(list_movie);
        $(".pagination li.current-page").on("click", async function () {
        
            selected_page = $(this).index();
            $('.pagination li').removeClass('active');
            $(this).addClass('active'); 
            Loading();
            var res = await getMovieByName(selected_page);
            var list_movie = res.results;
            fillMovie(list_movie);
        });
    }
    
}



function showPagination(m)
{
    $('#pagination').empty();
    $('#pagination').append(
        `
            <div class = "col-sm total-result">  Kết quả tìm kiếm cho '${$('form input').val()}': ${m.total_results} kết quả </div>
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li>
                        <a id = "previous-page" class="page-link" href="#" aria-disabled="true">Trước</a>
                    </li>
                    <li>
                        <a id ="next-page" class="page-link" href="#">Kế tiếp</a>
                    </li>
                </ul>
            </nav>`
    )

    for(i = m.total_pages; i >= 1; i--)
    {
        if(i == 1)
        {
            $(`<li class="page-item current-page active"><a class="page-link" href="#">${i}</a></li>`).insertAfter('.pagination li:first-child');
        }
        else
        {   
        $(`<li class="page-item current-page"><a class="page-link" href="#">${i}</a></li>`).insertAfter('.pagination li:first-child');
        }
    }
}

async function fillMovie(ms)
{
    $('#actor-details').empty();
    $('#movie-details').empty();
    $('#main').empty();
    for(const index of ms)
    {
        var genre_array = index.genre_ids;
        var genreString =  await buildGenreString(genre_array);
        $('#main').append(
            `   
            <div class="col-md-4 py-1" style="margin-top: -20px;">
                    <div class="card-custom shadow h-100" onclick="getMovieDetails(${index.id})">   
                        <div class="card-header-custom">
                            <img class="card-img-custom" src="${IMAGE_LINK}${index.poster_path}" alt="Card image"/>
                        </div>  
                        <div class="card-body-custom">
                            <h1 class="card-title-custom">${index.title}</h1>
                            <div class="container">
                                <div class="row">
                                    <div class="col-4 metadata-custom ">
                                        <p style="font-weight:bold" >Rated: ${index.vote_average}/10</p>
                                    </div>
                                    <div style="font-weight:bold" class="col-8 metadata-custom text-right">Genre: ${genreString} </div>
                                </div>
                            </div>      
                            <p class="card-text-custom">${index.overview}</p>
                        </div>
                    </div>
            </div>
            `
        )
    }
}

async function getMovieCast(id)
{
    var strRequest = `${GET_MOVIE_DETAILS_API}${id}/credits?${API_KEY}`;
    var response = await fetch(strRequest);
    var res = await response.json();
    return res;
}

async function getMovieDetails(id)
{
    var strRequest = `${GET_MOVIE_DETAILS_API}${id}?${API_KEY}`;
    var response = await fetch(strRequest);
    var movie_details = await response.json();
    var cast_and_crew = await getMovieCast(id);
    console.log(cast_and_crew);
    var review = await getMovieReview(id);
    fillMovieDetails(movie_details,cast_and_crew,review);
}

async function getMovieReview(id)
{
    var strRequest = `${GET_MOVIE_DETAILS_API}${id}/reviews?${API_KEY}`;
    var response = await fetch(strRequest);
    var res = await response.json();
    return res;
}

function fillMovieDetails(ms,cast_crew,review)
{
    $('#pagination').empty();
    $('#actor-details').empty();
    $('#main').empty();

    var list_review = review.results;
    var num_review = review.total_results;
    if(num_review == 0)
    {
        var author_0 = "";
        var content_0 = "Has not reviewed yet"; 
    }
    else{
        var author_0 = list_review[0].author;
        var content_0 = list_review[0].content;
    }

    var list_genre = "";
    for(i = 0;i<ms.genres.length;i++)
    {
        list_genre += ms.genres[i].name;
        list_genre += ", ";
    }

    var list_country = "";
    for(i = 0;i<ms.production_countries.length;i++)
    {
        list_country += ms.production_countries[i].name;
        list_country += ", ";
    }

    var list_company = "";
    for(i = 0;i<ms.production_companies.length;i++)
    {
        list_company += ms.production_companies[i].name;
        list_company += ", ";
    }

    var list_cast = [];
    var list_cast_id = [];
    if(cast_crew.cast.length > 0)
    {
        if(cast_crew.cast.length - 5 > 0)
        {
            for(i=0;i<5;i++)
            {
                var temp = cast_crew.cast[i].name;
                list_cast.push(temp);
                list_cast_id.push(cast_crew.cast[i].id);
            }
        }
        else{
            for(i=0;i<cast_crew.cast.length;i++)
            {
                var temp = cast_crew.cast[i].name;
                list_cast.push(temp);
                list_cast_id.push(cast_crew.cast[i].id);
            }
            for(i = cast_crew.cast.length; i<5;i++)
            {
                list_cast.push("");
                list_cast_id.push("");
            }
        }
    }
    else{
        for(i=0;i<5;i++)
        {
            list_cast.push("");
            list_cast_id.push("");
        }
    }
    
    var list_director = "";
    for(i =0;i<cast_crew.crew.length;i++)
    {
        if(cast_crew.crew[i].job == "Director")
        {
            list_director += cast_crew.crew[i].name;
            list_director += ", ";
        }
    }

    $('#movie-details').append(
        `
        <div class="col-md" style="margin-top: 20px;">
        <div class="card">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="${IMAGE_LINK}${ms.poster_path}" class="card-img" alt="..."/>
                </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">${ms.title}</h5>
                    <p class="card-text">Original title: ${ms.original_title}</p>
                <div class="imdb"> Vote Averate: ${ms.vote_average} </div>
                <div class ="details-title">Details: </div>
                <div>
                    <span class = "details-key">Release date: </span>
                    <span class = "details-value">${ms.release_date}</span>
                </div>
                <div>
                    <span class = "details-key">Genre: </span>
                    <span class = "details-value">${list_genre}</span>
                </div>
                <div>
                    <span class = "details-key">Directors: </span>
                    <span class = "details-value">${list_director}</span>
                </div>
                <div>
                    <span class = "details-key">Actors/Actress: </span>
                    <span class = "details-value">
                        <a href="#" onclick="getActorDetails(${list_cast_id[0]})"> ${list_cast[0]} </a>,
                        <a href="#" onclick="getActorDetails(${list_cast_id[1]})" > ${list_cast[1]} </a>,
                        <a href="#" onclick="getActorDetails(${list_cast_id[2]})"> ${list_cast[2]} </a>,
                        <a href="#" onclick="getActorDetails(${list_cast_id[3]})"> ${list_cast[3]} </a>,
                        <a href="#" onclick="getActorDetails(${list_cast_id[4]})"> ${list_cast[4]} </a>.
                    </span>
                </div>
                <div>
                    <span class = "details-key">Production Company: </span>
                    <span class = "details-value">${list_company}</span>
                </div>
                <div>
                    <span class = "details-key">Production Country: </span>
                    <span class = "details-value">${list_country}</span>
                </div>
                <div class ="details-title">Overview: </div>
                <p class="card-text">${ms.overview}</p>
                <div class = "details-title">Reviews: ${num_review} reviews </div>
                <div class="container" id="review_carousel">
                    <div id="carouselContent" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox" id ="review_content">
                            <div class="carousel-item active text-center p-4">
                                <p style="color: #888; font-size: 30px;"> Written by ${author_0} </p>
                                <p style="color: #888; font-size: 20px;">${content_0}</p>
                            </div>
                        </div>

                        <a class="carousel-control-prev" href="#carouselContent" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselContent" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

                
                </div>
            </div>
            </div>
        </div>
        </div>
        `
    );
    
    
    if(num_review > 1)
    {
        fillReview(list_review);
    } 
}


function fillReview(list_review)
{
    for(i =1;i<list_review.length;i++)
    {
        $('#review_content').append(
            `
            <div class="carousel-item text-center p-4">
                <p style="color: #888; font-size: 30px;"> Written by ${list_review[i].author} </p>
                <p style="color: #888; font-size: 20px;">${list_review[i].content}</p>
            </div>
            `
        )
    }
}

async function getActorDetails(id)
{
    var strRequest = `${GET_ACTOR_DETAILS_API}${id}?${API_KEY}`;
    var response = await fetch(strRequest);
    var res = await response.json();
    
    var list_movie = await getMovieCreditsForPerson(id);
    showActorDetails(res,list_movie);
}

async function getMovieCreditsForPerson(id)
{
    var strRequest = `${GET_MOVIE_CREDITS_FOR_PERSON_API}${id}/movie_credits?${API_KEY}`;
    var response = await fetch(strRequest);
    var res = await response.json();
    
    return res.cast;
}



async function showActorDetails(res,list_movie)
{

    $('#movie-details').empty();
    $('#actor-details').append(
        `
        <div class="col-md" style="margin-top: 20px;">
        <div class="card">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="${IMAGE_LINK}${res.profile_path}" class="card-img" alt="..."/>
                </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">${res.name}</h5>
                    <p class="card-text">Also known as: ${res.also_known_as[0]}</p>
                <div class="imdb"> Popularity: ${res.popularity} </div>
                <div class ="details-title">Details: </div>
                <div>
                    <span class = "details-key">Date of birth: </span>
                    <span class = "details-value">${res.birthday}</span>
                </div>
                <div>
                    <span class = "details-key">Deathday: </span>
                    <span class = "details-value">${res.deathday}</span>
                </div>
                <div>
                    <span class = "details-key">Place of birth: </span>
                    <span class = "details-value">${res.place_of_birth}</span>
                </div>
                <div>
                    <span class = "details-key">Gender: </span>
                    <span class = "details-value">${res.gender} (1: Female, 2: Male)</span>
                </div>
                <div class ="details-title">Biography: </div>
                    <p class="card-text">${res.biography}</p>
                </div>
            </div>
            </div>
        </div>
        </div>
        `
    );
    $('#actor-details').append(
        `
        <div class="col-md text-center" style="margin-top: 30px;">
            <div class="details-title" style="font-size:35px;"> Movies that have ${res.name}</div>
        </div>
        <div id ="movie-with-actor" class="card-deck">

        </div>
        `
    )
    for(const index of list_movie)
    {
        $('#movie-with-actor').append(
            `
            <div class="col-md-4 py-1" style="margin-top: -20px;">
            <div class="card-custom shadow h-100" onclick="getMovieDetails(${index.id})">   
                <div class="card-header-custom">
                    <img class="card-img-custom" src="${IMAGE_LINK}${index.poster_path}" alt="Card image"/>
                </div>  
                <div class="card-body-custom">
                    <h1 class="card-title-custom">${index.title}</h1>
                    <div class="container">
                        <div class="row">
                            <div class="col-4 metadata-custom ">
                                <p style="font-weight:bold" >Rated: ${index.vote_average}/10</p>
                            </div>
                            
                        </div>
                    </div>      
                    <p class="card-text-custom">${index.overview}</p>
                </div>
            </div>
            </div>
            `
        )
    }
}